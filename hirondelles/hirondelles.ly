#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)

\relative{
    \tempo "" 4 = 100 \clef "treble" \key c \major \time 4/4
    \partial 4
    e'8 d
    \repeat volta 2 { c2 (c8) e f g g d (d2) e8 d
    c2. d8 e e b (b2) c8 b
    a2 (a8) a b c c2. c8 c
    d4 a b c d4 r2 e8 d }

}
    \addlyrics {
        T'en fais pas la vie est bel -- le,
        com' un vol d'hi -- ron -- del -- les,
        qui s'en va, et qui re -- vient,
        au prin -- temps, un beau ma -- tin, T'en fais
  }




