#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)

\relative{
    \tempo "" 2 = 55 \clef "treble" \key bes \major \time 2/2
    \mark "A"
    \repeat volta 2 {f'4. d8 f4. d8 f g f2 bes,8 c d d d d (d) c c4 |}
        
        \alternative{
            { bes1 }
            { bes2 (bes8) bes c4 }
        }
    
    \mark "B"
    \repeat volta 2 {d2 (d8) c d4 ees2. d8 ees f f f f (f) ees ees4}
        \alternative{
            {d2 (d8) bes c4}
            {d1}
        }
    
    \mark "C"
    \repeat volta 2 {c'2. bes4 bes a2 g8 a bes bes bes bes (bes) a a4 g1}


}

    \addlyrics {
        si si si si do la da,
        ya cou -- ssi -- né la dou ba -- na -- ha, ha
        ba -- na
        ha, ba -- na -- ha, ya cou -- ssi -- né la dou ba -- na -- ha, ba -- na ha
        na ba -- na -- ha, ya cou -- ssi -- né la dou ba -- na -- ha, ha
        
    }


