#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)


\relative{
    \tempo "" 4 = 105 \clef "treble" \key c \major \time 4/4 
    \partial 2
    \mark "A" c'8 d e c 
    \repeat volta 2 {
    a2 b2 | c4 r4 \mark "B" c8 d e c |
    f2 g2 | e4 r4 \mark "C" e8 f g e |
    a4 a d,8 e f d | g4 g c,8 d e c |
    a2 b2 | c4 r4 c8 d e c 
    }
}
\addlyrics {
U -- ne bul -- le, bul -- le vole,
Qua -- tre bul -- les, bulles s'en -- volent,
Des mil -- liers de bul -- les vont en fa -- ran -- do -- le,
U -- ne bul -- le, bul -- le vole,
U -- ne bul -- le,
}
