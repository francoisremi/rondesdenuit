#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)


\relative{
    \tempo "" 2 = 55 \clef "treble" \key f \major \time 2/2  \partial 2 \mark "A" d'4 e  \repeat volta 2 { f4. e8 f4. e8 f4. e8 f e f g | e4. d8 e d e f e4 g f e | d2 \mark "B" f4 g  a4. g8 a4. g8 a4.g8 a g a bes g4. f8 g f g a g4 bes a g f \mark "C" f f e  d2 (f2 a4) d c4. bes8 c1 (c4) g a4. bes8 a1 (a4) \mark "D" f f e  d2 (f e1) (e4) d d c d2 d4 e }
}
\addlyrics {
Dans le port de Lo -- ri -- ent, de grands voi -- liers tout blancs,
A -- tten -- dent en rê -- vant, au gré du vent,
Dans le port de Lo -- ri -- ent, de grands voi -- liers tout blancs,
A -- tten -- dent en rê -- vant, au gré du vent,
Puis ils s’en vont pour l’O -- ri -- ent, au gré du vent,
Puis ils s’en vont, au gré du vent. Dans le
}
