# Rondes de nuit

## Requirements

- Lilypond
- XeLatex

## Compile

> chmod +x compile.sh
> ./compile.sh

## ajouter une chanson

Bien penser à mettre ceci au début du code lilypond:

    #(set-default-paper-size "a5landscape")
    \paper {
    left-margin = 0
    right-margin = 30
    indent=0
    }
    #(set-global-staff-size 18)
    
La taille des portées (ici 18) est à définir officiellement.
    
Puis, l'ajouter au fichier `laronde.lytex`:

    \begin{chanson}{Le port de Lorient}{Anonyme}{Canon irrégulier}
    \lilypondfile[quote, noindent]{lorient/lorient.ly}
    \end{chanson}
    
## Remarques

Bien indiquer la provenance de la transmission et l'auteurice le cas échéant.
