#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)


\relative{
    \tempo "" 4 = 90 \clef "treble" \key c \major \time 3/4 
    \partial 4
    a4
    \repeat volta 2 {
    e'4. e8 d e |
    c4 c8 r a a |
    c4. c8 d c |
    e4 e8 r e e |
    a4. a8 g a |
    e4 e8 r4. |
    e4 e8 r4 e8 |
    a,4 r4 a4
    }
}
\addlyrics {
Bon -- soir tous à la ron -- de,
Que la paix soit sur le mon -- de,
Dans la nuit calme et se -- rei -- ne,
L'hom -- me, sans hai -- ne
}
