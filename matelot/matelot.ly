#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)

\relative{
    \tempo "" 4 = 100 \clef "treble" \key g \major \time 3/4
    \partial 4
    \mark "A"
    g'8. a16
    \repeat volta 2 {
    b4 b8. g16 b8. c16 b4 (b8.) b16 c8. b16
    a4 (a8.) c16 b8. a16 a4 g4 g8. a16
    b4 b8. g16 b8. c16 b2 c8. b16 
    a4 a8. c16 b8. a16 g4 r4 \mark "B" b8. c16
    d4 d8. b16 e8. d16 d2 e8. d16 c4 a8. d16 d8. d16 b4 g4 b8. c16
    d4 d8. b16 e8. d16 d4 (d8.) d16 e8. d16 c4 a8. d16 d8. d16 g,4 r4 g8. a16
    }
}

    \addlyrics {
    Ma -- te -- lot puis -- qu'il fait bon vent, pou -- sons ce soir la chan -- so -- net -- te,
    Ma -- te -- lot puis -- qu'il fait bon vent, mon -- tons tous chan -- ter sur l'a -- vant,
    Et le chant du gail -- lard d'a -- vant, mon -- te -- ra jus -- qu'à la lu -- net -- te,
    Et le chant du gail -- lard d'a -- vant, é -- gail -- le -- ra tout le bâ -- ti -- ment.
    }


