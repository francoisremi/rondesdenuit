#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)


\relative{
\tempo "" 4. = 50 \clef "treble" \key c \major \time 6/8  \mark "A" c'4 c8 d4 d8 g4 g8 e4 e8 \mark "B" e16 e e e e e g4. d16 e f e d f e4. \mark "C" c16 c c c c c b4. b16 c d c b d c4. \bar ":|."
}

\addlyrics {
Buo -- na -- se -- ra, Buo -- na -- se -- ra,
Gia la luna app -- are in cielo,
et si sen -- te ro -- ssi -- gnol,
Gia la luna app -- are in cielo,
et si sen -- te ro -- ssi -- gnol
}
