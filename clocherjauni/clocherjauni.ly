#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)


\relative{
    \tempo "" 4. = 60 \clef "treble" \key c \major \time 6/8
    \partial 8
    g16 g
    \repeat volta 2 {
    \mark "A"
        c8 c c e c g16 g c8 b c d4 g,16 g d'8 d d f d g,16 g g8 a b c4.
    \mark "B"
        e8 e8. e16 g8 e8. e16 e8 d e f4. f8 f8. f16 a8 f8. g16 g8 g8 f8 e4 g,16 g
    }
}
\addlyrics {
C'é -- tait dans la nuit bru -- ne,
sur un clo -- cher jo -- li,
sur un clo -- cher la lu -- ne,
com' un point sur un i.
Ya -- la hi hi hi hi ya -- la hi ho,
Ya -- la hi hi hi hi ya -- la hi ho. C'é -- tait
}
