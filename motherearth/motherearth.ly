#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)

\relative{
<<
  \new Staff { \tempo "" 4 = 55 \clef "treble" \key c \major \time 2/2  a4. a8 b4. b8 c (b) c (d) e2 a4 a g a8 g e4 a a2 a,4. c8 b4. d8 c (b) c (d) e4 e8 e a4 a g a8 (g) e1  \bar ":|."}
\addlyrics {
Eve -- ning ris -- es, Spi -- rits come,
Sun goes down when the day is done,
Mo -- ther Earth a -- wa -- kens me with the hard bit of the see 
}
\new Staff { \clef "treble" \key c \major \time 2/2  a,4 e'4 e4. e8 d4 e a,2 c4 c b c8 b a4 e' e2 a,4 e' e4. e8 d4 e a, a8 b8 c4 c b c8 (b) a1 \bar ":|."}
\addlyrics {
Eve -- ning ris -- es, Spi -- rits come,
Sun goes down when the day is done,
Mo -- ther Earth a -- wa -- kens me with the hard bit of the see 
}
\new Staff { \clef "bass" \key c \major \time 2/2  c,4 c b2 a4 a g2 f4 f g e8 g a4 c c2 \bar ":|."}
>>
}


