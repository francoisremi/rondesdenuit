#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-last = ##t
}
#(set-global-staff-size 18)


\relative{
    \tempo "" 2 = 50 \clef "treble" \key f \major \time 4/2  \repeat volta 2 { \mark "A" d'4 f e d c e8 e d2 d4 f e d c e8 e d2 \mark "B" a'4 a bes bes bes8 d c bes d4 a f f g f e a8 g a2 \mark "C" a4 a bes bes bes8 d c bes d4 a f f g f e a8 g d2 }
}

\addlyrics {
Ter -- re rou -- ge Ter -- re de feu,
Ter -- re rou -- ge Ter -- re de feu,
Ter -- re Ter -- re Ter -- re de lu -- mi -- ère,
Ter -- re rou -- ge sous le ciel bleu,
Ter -- re Ter -- re Ter -- re de lu -- mi -- ère,
Ter -- re rou -- ge sous le ciel bleu
}
