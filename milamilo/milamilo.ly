#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-right = ##t
}
#(set-global-staff-size 18)


\relative{
\tempo "" 4 = 100 \clef "treble" \key ees \major \time 5/4  \mark "A" g'8 g4 g8 aes4. g16 f g8 ees \mark "B" ees8 ees4 ees8 f4. ees16 d ees8 c \break \mark "C" c c16 c c8 c16 c d8 c r2 \mark "D" g'8 c,16 c g'8 c,16 c g'8 g r2 \bar ":|."
}

\addlyrics {
Mi -- la mi -- lo mi -- lo camp -- bel,
Mi -- la mi -- lo mi -- lo camp -- bel,
Mi -- la mi -- lo mi -- lo camp -- bel,
Mi -- la mi -- lo mi -- lo camp -- bel
}
