lilypond-book --pdf --format=latex --output=out rondes_de_nuit.lytex --include=part
cd out
pdflatex rondes_de_nuit.tex
mv rondes_de_nuit.pdf ../rondes_de_nuit.pdf
cd ../
