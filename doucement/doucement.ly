#(set-default-paper-size "a5landscape")
\paper {
  left-margin = 0
  right-margin = 30
  indent=0
  ragged-right = ##t
}
#(set-global-staff-size 18)

\relative{
  	

<<
  \new Staff { \tempo "" 4 = 42 \clef "treble" \key e \major cis'8 e8 dis8 b8 cis8 e8 dis4 | e8 e8 fis8 b16 (a16) gis2 | gis8 b16 (gis16) fis16 (a16) gis16 (fis16) e8 e8 dis4 | cis16 dis16 e16 fis16 gis8 gis8 cis,2 \bar "|."}
    \addlyrics {
    Dou -- ce -- ment s'en -- dort la Terre, Dans le soir tom __ bant, Fer -- me vi -- te tes pau -- pières, Dors mon tout pe -- tit en -- fant.
  }
  \new Staff { \clef "bass" \key e \major cis,8 cis8 b8 b8 a8 a8 b4 | cis8 cis8 dis8 dis8 e2 | e8 e8 dis8 dis8 cis8 cis8 bis4 | a16 a16 a16 a16 gis8 gis8 cis2 \bar "|." }

>>

  
}
